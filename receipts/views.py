from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm
from django.contrib.auth.decorators import login_required


@login_required
def home(request):
    user = request.user
    filtered_receipt = []
    receipt = Receipt.objects.all()
    for r in receipt:
        if r.purchaser == user:
            filtered_receipt.append(r)
    context = {
        "user": user,
        "home": filtered_receipt,
    }
    return render(request, "receipts/home.html", context)


@login_required
def create_receipt(request):
    user = request.user
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
        form.fields["category"].queryset = ExpenseCategory.objects.filter(
            owner=user
        )
        form.fields["account"].queryset = Account.objects.filter(owner=user)
    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {"category_list": categories}
    return render(request, "receipts/category_list.html", context)


@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {"account_list": accounts}
    return render(request, "receipts/account_list.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            expensecategory = form.save(False)
            expensecategory.owner = request.user
            expensecategory.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/categories/create_category.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()

    context = {
        "form": form,
    }
    return render(request, "receipts/accounts/create_account.html", context)
